try:
    from unittest.mock import MagicMock
except ImportError:
    from mock import MagicMock

import tango

from dsconfig.json2tango import apply_config


original_config = {
    "servers": {
        "TangoTest": {
            "test": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {}
                }
            }
        }
    }
}
config = {
    "servers": {
        "TangoTest": {
            "test": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {
                        "alias": "hello"
                    },
                }
            }
        }
    }
}

config2 = {
    "servers": {
        "TangoTest": {
            "test": {
                "TangoTest": {
                    "sys/tg_test/1": {},
                    "sys/tg_test/2": {},
                    "sys/tg_test/3": {
                        "alias": "hello"
                    },
                }
            }
        }
    }
}


def test_apply_config__ignores_noncolliding_alias():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["alias2", "some/other/device"]
    _, _, dbcalls, _, alias_collisions = apply_config(config, db, original=original_config)
    assert dbcalls == [
        ("put_device_alias", ("sys/tg_test/2", "hello",), {})
    ]


def test_apply_config__removes_colliding_alias():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["hello", "sys/tg_test/1",
                                         "alias2", "some/other/device"]
    _, _, dbcalls, _, alias_collisions = apply_config(config, db, original=original_config)
    assert dbcalls == [
        ("delete_device_alias", ("hello",), {}),
        ("put_device_alias", ("sys/tg_test/2", "hello",), {})
    ]


def test_apply_config__removes_colliding_alias_new_device():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["hello", "some/other/device"]
    _, _, dbcalls, _, alias_collisions = apply_config(config2, db, original=original_config)
    assert len(dbcalls) == 3
    assert dbcalls[0] == ('delete_device_alias', ('hello',), {})
    assert dbcalls[1][0] == "add_device"
    assert dbcalls[2] == ("put_device_alias", ("sys/tg_test/3", "hello",), {})


def test_apply_config__remove_alias():
    db = MagicMock(spec=tango.Database)
    db.command_inout.return_value = [], ["hello", "sys/tg_test/1"]
    _, _, dbcalls, _, alias_collisions = apply_config(original_config, db, original=original_config)
    assert dbcalls == [('delete_device_alias', ('hello',), {})]
