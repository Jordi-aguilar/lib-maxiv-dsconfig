import json

import tango
import pytest
from dsconfig.tangodb import get_dict_from_db, get_servers_with_filters, get_classes_properties
from dsconfig.utils import ObjectWrapper, find_device
try:
    from unittest.mock import Mock, MagicMock, create_autospec
except ImportError:
    from mock import Mock, MagicMock, create_autospec


def make_db(dbdata):
    """ Return a fake tango DB that returns stuff based on the given dict. """

    db = create_autospec(tango.Database)

    def get_device_info(dev):
        _, (srv, inst, clss, _) = find_device(dbdata, dev)
        return MagicMock(ds_full_name="%s/%s" % (srv, inst), name=dev,
                         class_name=clss)

    db.get_device_info.side_effect = get_device_info

    def get_device_name(server, clss):
        srv, inst = server.split("/")
        return list(dbdata["servers"][srv][inst][clss].keys())

    db.get_device_name.side_effect = get_device_name

    def get_device_property_list(dev, pattern):
        data, _ = find_device(dbdata, dev)
        return list(data["properties"].keys())

    db.get_device_property_list.side_effect = get_device_property_list

    def get_device_property(dev, props):
        data, _ = find_device(dbdata, dev)
        return dict((p, data["properties"][p]) for p in props)

    db.get_device_property.side_effect = get_device_property

    def get_alias_from_device(dev):
        data, _ = find_device(dbdata, dev)
        return data.get("alias")

    db.get_alias_from_device.side_effect = get_alias_from_device

    return db


def assert_json_dumpable(value):
    """ Check that the value is JSON compatible """
    assert json.dumps(value)


def test_get_dict_from_db():
    indata = {
        "servers": {
            "testserver": {
                "testinstance": {
                    "testclass": {
                        "a/b/c": {}
                    }}}}}

    dbdata = {
        "servers": {
            "testserver": {
                "testinstance": {
                    "testclass": {
                        "a/b/c": {
                            "alias": "bananas",
                            "properties": {
                                "a": ["78", "103"]
                            }}}}}}}

    db = make_db(dbdata)
    d, moved = get_dict_from_db(db, indata)
    assert d == dbdata
    assert_json_dumpable(d)


def test_get_props_with_mixed_case():
    db = create_autospec(tango.Database)
    query_results = [
        (None, [
            "a/b/c", "prop1", "prop1 line 1",
            "A/B/C", "prop1", "prop1 line 2",
            "A/b/C", "prop1", "prop1 line 3",
            "A/B/c", "prop2", "prop2 line 1",
            "a/B/C", "prop2", "prop2 line 2",
        ]),
        (None, [
            "TangoTest/1", "TangoTest", "A/B/C", "some_alias"
        ])
    ]
    db.command_inout = Mock(side_effect=query_results)
    data = get_servers_with_filters(attribute_properties=False, dbproxy=db)
    data = data.to_dict()
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["properties"]["prop1"] == [
        "prop1 line 1",
        "prop1 line 2",
        "prop1 line 3"]
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["properties"]["prop2"] == [
        "prop2 line 1",
        "prop2 line 2"]
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["alias"] == "some_alias"
    assert_json_dumpable(data)


def test_get_attr_props_with_mixed_case():
    db = create_autospec(tango.Database)
    query_results = [
        (None, [
            "a/b/c", "SomeAttr", "prop1", "prop1 line 1",
            "A/B/C", "SomeAttr", "prop1", "prop1 line 2",
            "a/B/C", "SomeAttr", "prop1", "prop1 line 3",
            "A/b/C", "SomeAttr", "prop2", "prop2 line 1",
            "A/B/c", "SomeAttr", "prop2", "prop2 line 2",
            "A/b/C", "SomeOtherAttr", "prop3", "prop3 line 1",
            "A/B/c", "SomeOtherAttr", "prop3", "prop3 line 2",

        ]),
        (None, [
            "TangoTest/1", "TangoTest", "A/B/C", "an_alias"
        ])
    ]
    db.command_inout = Mock(side_effect=query_results)
    data = get_servers_with_filters(properties=False, dbproxy=db)
    data = data.to_dict()
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["attribute_properties"]["SomeAttr"]["prop1"] == [
        "prop1 line 1",
        "prop1 line 2",
        "prop1 line 3"]
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["attribute_properties"]["SomeAttr"]["prop2"] == [
        "prop2 line 1",
        "prop2 line 2"]
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["attribute_properties"]["SomeOtherAttr"]["prop3"] == [
        "prop3 line 1",
        "prop3 line 2"]
    assert data["TangoTest"]["1"]["TangoTest"]["A/B/C"]["alias"] == "an_alias"
    assert_json_dumpable(data)


def test_get_classes_properties():
    db = create_autospec(tango.Database)
    query_results = [
        (None, [
            "SnapArchiver", "beansFileName", "beansMAXIV.xml", 1,
            "SnapArchiver", "DbHost", "localhost", 1,
            "SnapArchiver", "DbSchemaList", "schema1", 1,
            "SnapArchiver", "DbSchemaList", "schema2", 2
        ]),
    ]
    db.command_inout = Mock(side_effect=query_results)
    data = get_classes_properties(dbproxy=db, cls_attribute_properties=False)
    data = data.to_dict()
    assert data["SnapArchiver"]["properties"]["beansFileName"] == ["beansMAXIV.xml"]
    assert data["SnapArchiver"]["properties"]["DbHost"] == ["localhost"]
    assert data["SnapArchiver"]["properties"]["DbSchemaList"] == ["schema1", "schema2"]
    assert_json_dumpable(data)


def test_get_classes_attribute_properties():
    db = create_autospec(tango.Database)
    query_results = [
        (None, [
            "TangoTest", "ampli", "prop1", "hello", 1,
            "TangoTest", "ampli", "prop1", "1", 2,
            "TangoTest", "ampli", "prop1", "0.2", 3,
            "Motor", "position", "prop1", "0.2", 1
        ]),
    ]
    db.command_inout = Mock(side_effect=query_results)
    data = get_classes_properties(cls_properties=False, dbproxy=db)
    data = data.to_dict()
    print(data)
    assert data["TangoTest"]["attribute_properties"]["ampli"]["prop1"] == [
        "hello",
        "1",
        "0.2"]
    assert data["Motor"]["attribute_properties"]["position"]["prop1"] == ["0.2"]
    assert_json_dumpable(data)
