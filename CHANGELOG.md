# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [1.7.1] - 2023-11-16

### Fixed
- Fix a bug introduced by a broken merge, preventing the new feature of 1.7.0 from working.
- Respect the tango host when passing a DB device name to get_db_data() (Thanks @alexhill)


## [1.7.0] - 2023-11-15

Started logging changes in this file!

### Added
- Introduce option to force deletion of "protected" properties

### Fixed
- Better handling of device alias collisions

